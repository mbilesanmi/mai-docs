[![Coverage Status](https://coveralls.io/repos/github/andela-milesanmi/mai-docs/badge.svg?branch=develop)](https://coveralls.io/github/andela-milesanmi/mai-docs?branch=master)
[![Code Climate](https://codeclimate.com/github/andela-milesanmi/mai-docs/badges/gpa.svg)](https://codeclimate.com/github/andela-milesanmi/mai-docs/)
[![Build Status](https://travis-ci.org/andela-milesanmi/mai-docs.svg?branch=master)](https://travis-ci.org/andela-milesanmi/mai-docs)
[![license](https://img.shields.io/github/license/mashape/apistatus.svg)]()

# Mai Docs
Mai Docs is a full stack document management system.
